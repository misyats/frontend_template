var gulp 			= require('gulp'),
		sass 			= require('gulp-sass'),
		browserSync 	= require('browser-sync'),
		concat 			= require('gulp-concat'),
		uglify			= require('gulp-uglifyjs'),
		cssnano			= require('gulp-cssnano'),
		rename			= require('gulp-rename')
		del 			= require('del'),
		imagemin		= require('gulp-imagemin'),
		pngquant		= require('imagemin-pngquant'),
		cache			= require('gulp-cache'),
		autoprefixer 	= require('gulp-autoprefixer'),
		mainBowerFiles	= require('main-bower-files'),
		spritesmith 	= require('gulp.spritesmith'),
		buffer 			= require('vinyl-buffer'),
		fileinclude 	= require('gulp-file-include'),
		gulpRemoveHtml 	= require('gulp-remove-html'),
		merge 			= require('merge-stream');

gulp.task('sass', function() {
	return gulp.src('app/sass/**/*.{scss,sass}')
		.pipe(sass({
			includePaths: require('node-bourbon').includePaths
		}).on('error', sass.logError))
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
		.pipe(cssnano())
		.pipe(gulp.dest('app/css'))
		.pipe(browserSync.reload({stream: true}))
});

// gulp.task('headersass', function() {
// 	return gulp.src('app/header.sass')
// 		.pipe(sass({
// 			includePaths: require('node-bourbon').includePaths
// 		}).on('error', sass.logError))
// 		.pipe(rename({suffix: '.min', prefix : ''}))
// 		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
// 		.pipe(cssnano())
// 		.pipe(gulp.dest('app'))
// 		.pipe(browserSync.reload({stream: true}))
// });

gulp.task('scripts', function() {
	return gulp.src([
		'app/libs/jquery/dist/jquery.min.js',
		'app/libs/owl.carousel/dist/owl.carousel.min.js',
		'app/libs/imagesloaded/imagesloaded.pkgd.min.js',
		'app/libs/jQuery.equalHeights/jquery.equalheights.min.js',
		'app/libs/fancyBox/dist/jquery.fancybox.min.js',
	])
	.pipe(concat('libs.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('app/js'));
});

// gulp.task('css-libs', ['sass'], function() {
// 	return gulp.src('app/css/libs.css')
// 	.pipe(cssnano())
// 	.pipe(rename({suffix: '.min'}))
// 	.pipe(gulp.dest('app/css'));
// });

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false
	})
});

gulp.task('clean', function() {
	return del.sync('dist');
});

gulp.task('clear', function() {
	return cache.clearAll();
});

gulp.task('img', function() {
	return gulp.src('app/img/**/*')
		.pipe(cache(imagemin({
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			une: [pngquant()]
		})))
		.pipe(gulp.dest('dist/img'));
});

gulp.task('watch', ['browser-sync', 'scripts'], function() {
	gulp.watch('app/sass/**/*.{scss,sass}', ['sass']);
	gulp.watch('app/*.html', browserSync.reload);
	gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('buildhtml', function() {
	gulp.src(['app/*.html'])
		.pipe(fileinclude({
			prefix: '@@'
		}))
		.pipe(gulpRemoveHtml())
		.pipe(gulp.dest('dist/'));
});

gulp.task('build', ['clean', 'buildhtml', 'img', 'sass', 'scripts'], function() {
	var buildCss = gulp.src([
		'app/css/*.css',
		// 'app/css/libs.min.css',
	])
	.pipe(gulp.dest('dist/css'));

	var buildFonts = gulp.src('app/fonts/**/*')
		.pipe(gulp.dest('dist/fonts'));

	var buildJs = gulp.src('app/js/**/*')
		.pipe(gulp.dest('dist/js'));

	// var buildHtml = gulp.src('app/*.html')
	// 	.pipe(gulp.dest('dist'));
});

gulp.task('sprite', function () {
	// Generate our spritesheet
	var spriteData = gulp.src('app/img/icons/*.png').pipe(spritesmith({
		imgName: 'sprite.png',
		cssName: '_sprite.sass',
		imgPath: '../img/sprite.png',
		padding: 2
	}));

	// Pipe image stream through image optimizer and onto disk
	var imgStream = spriteData.img
		// DEV: We must buffer our stream into a Buffer for `imagemin`
		.pipe(buffer())
		.pipe(imagemin())
		.pipe(gulp.dest('app/img/'));

	// Pipe CSS stream through CSS optimizer and onto disk
	var cssStream = spriteData.css
		// .pipe(csso())
		.pipe(gulp.dest('app/sass/'));

	return merge(imgStream, cssStream);
});

gulp.task('default', ['browser-sync', 'watch']);
